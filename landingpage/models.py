from django.db import models

# Create your models here.

class Status(models.Model):
	status = models.TextField(blank=False, max_length=300)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		ordering = ('-created_at',)