from django.urls import path

from .views import index, add_status
# delete_status

app_name = 'landingpage'

urlpatterns = [
    path('', index, name = 'index'),
    path('add_status/', add_status, name='add_status'),
    # path('delete_status/<int:status_id>', delete_status, name='delete_status'),
]