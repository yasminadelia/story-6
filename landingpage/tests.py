from django.test import TestCase
from django.test import Client
from django.contrib import admin 
from django.urls import resolve
from django.http import HttpRequest
from .apps import LandingpageConfig
from .views import index, add_status
from .models import Status
from .forms import Status_Form

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
    

class LandingPageUnitTest(TestCase):

	def test_landingpage_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_landingpage_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_page_is_not_exist(self):
		self.response = Client().get('wek/')
		self.assertEqual(self.response.status_code, 404)


	def test_add_status_using_func(self):
		found = resolve('/add_status/')
		self.assertEqual(found.func, add_status)

	def test_hi_how_are_you_today(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hi, How are you today?', html_response)

	def test_index_use_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'landingpage.html')

	def test_index_does_not_have_status(self):
		response = Client().get('/')
		self.assertContains(response, "There's no status yet.")


	def test_index_have_status(self):
		# Creating a new activity
		new_status = Status.objects.create(status='tes')
		new_status.save()
		# Retrieving all available activity
		response = Client().get('/')
		self.assertContains(response, 'tes')

	def test_index_add_status_valid(self):
		response = Client().post('/add_status/', {'status':'Halo semua'})
		self.assertRedirects(response, '/')
        
		response2 = Client().get('/')
		self.assertContains(response2, 'Halo semua')

	def test_index_add_status_not_valid(self):
		response = Client().post('/add_status/', {})
		self.assertRedirects(response, '/')

		response2 = Client().get('/')
		self.assertContains(response2, "There's no status yet.")

class LandingpageFormTest(TestCase):
	def test_forms_valid(self):
		data = {'status': 'Halo semua'}
		new_status = Status_Form(data = data)
		self.assertTrue(new_status.is_valid())
		self.assertEqual(new_status.cleaned_data['status'], 'Halo semua')

	def test_form_is_not_valid(self):
		form = Status_Form(data = {})
		self.assertFalse(form.is_valid())



class StatusModelsTest(TestCase):
	@classmethod
	def setUpTestData(cls):
		Status.objects.create(status="a status")

	def test_if_status_created(self):
		count = Status.objects.all().count()
		self.assertEqual(count, 1)
    
	def test_if_status_exist(self):
		status = Status.objects.get(id=1)
		status_fill = status.status
		self.assertEqual(status_fill, "a status")

	def test_if_status_max_length_is_300(self):
		status = Status.objects.get(id=1)
		status_length = status._meta.get_field('status').max_length
		self.assertEqual(status_length, 300)

	def test_if_new_is_in_database(self):
		new_status = Status(status = 'tes')
		new_status.save()
		count = Status.objects.all().count()
		self.assertEqual(count, 2)

	

class LandingPageFunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		self.selenium  = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
		super(LandingPageFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(LandingPageFunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
        # Opening the link we want to test
		selenium.get('http://localhost:8000/')
		time.sleep(5) 
        # find the form element
		status = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('submit')

        # Fill the form with data
		status.send_keys('coba coba')

        # submitting the form
		submit.send_keys(Keys.RETURN)
		time.sleep(10)

