from django import forms
from django.forms import Form

class Status_Form(forms.Form):

	error_messages = {
        'required': 'You should write your status.',
    }


	pos_attrs = {
		'type': 'text',
		'class':'form-control',
		'placeholder':'Write here... (300 characters)'

	}
	status = forms.CharField(required=True, widget=forms.Textarea(attrs=pos_attrs), max_length=300)
	