from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status



# Create your views here.

def index(request):
	
	response = {"status_form": Status_Form}
	statuses = Status.objects.all().values()
	response['status'] = statuses
	return render(request, 'landingpage.html', response)

def add_status(request):
	
	if(request.method == 'POST'):
		form = Status_Form(request.POST)
		if (form.is_valid()):
			status_fill = form.cleaned_data['status']
			status = Status(status = status_fill)
			status.save()
			
			
		return HttpResponseRedirect('/')
		


